from .lupi_mfa import LupiMFA



__version__ = 'v6.0.0'
__author__ = 'VencoreLabs'

__all__ = ['LupiMFA']

from pkgutil import extend_path
__path__ = extend_path(__path__, __name__)  # type: ignore

